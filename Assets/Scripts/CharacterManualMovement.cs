﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterManualMovement : MonoBehaviour
{

    private Rigidbody2D rb;

    private Animator animator;
    private bool facingRight = true;
    public float jumpForce = 15.0f;
    public bool isGrounded;
    [SerializeField]
    private int maxLifes = 3;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            isGrounded = false;
            rb.AddForce(Vector3.up * jumpForce, ForceMode2D.Impulse);
        }
    }
    private void FixedUpdate()
    {
        float move = Input.GetAxis("Horizontal");
        rb.velocity = new Vector2(move*5, rb.velocity.y);


        if ((move > 0 && !facingRight) || (move < 0 && facingRight))
        {
            Flip();
        }
        if(move != 0)
        {
            animator.Play("BlobPlayer_Blue_Walk");
        }

        if (move == 0)
        {
            animator.Play("BlobPlayer_Blue_Idle");
        }

    }

    void OnCollisionStay2D()
    {
        isGrounded = true;
    }
    void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.gameObject.tag == "BottomLimit")
        {
            GameManager.Instance.PlayerHit();
        }
        if(collider.gameObject.tag == "Heart" && GameManager.Instance.GetPlayerLifes() < maxLifes)
        {
            GameManager.Instance.PlayerGetLife();
            Destroy(collider.gameObject);
        }
    }
    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
