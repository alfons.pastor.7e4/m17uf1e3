﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[System.Serializable]
public enum Direction
{
    UP,
    DOWN,
    RIGHT,
    LEFT,
}
[System.Serializable]
public struct Maneuver
{
    public Direction direction;
    public float distance;
    public Maneuver(Direction _direction, float _distance)
    {
        this.direction = _direction;
        this.distance = _distance;
    }
}

public class CharacterMovement : MonoBehaviour
{
    public bool active = false;
    public Sprite[] spriteArray;
    private int maneuverCounter = 0;
    public CharacterAnimation characterAnimation;
    public PlayerData playerData;
    private Vector3 targetPosition;
    public Maneuver[] maneuvers;
    private float step;
    public bool loopManeuvers = true;

    // Start is called before the first frame update
    void Start()
    {
        targetPosition = transform.position;
        characterAnimation = GetComponentInParent<CharacterAnimation>();
        playerData = GetComponentInParent<PlayerData>();
    }

    // Update is called once per frame
    void Update()
    {
        if (active)
        {
            ManageManeuvers();
            ManageAnimation();
            step = playerData.speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, step);
        }
    }

    void ManageManeuvers()
    {
        if (HasFinishedMoving && maneuverCounter < maneuvers.Length)
        {
            if(maneuverCounter < maneuvers.Length)
            NewTarget(maneuvers[maneuverCounter]);
            maneuverCounter++;
        }
        if((maneuverCounter == maneuvers.Length) && loopManeuvers)
        {
            maneuverCounter = 0;
        }
    }

    void ManageAnimation()
    {
        characterAnimation.isMoving = !HasFinishedMoving;
    }

    bool HasFinishedMoving => transform.position == targetPosition;

    void NewTarget(Maneuver maneuver)
    {
        switch (maneuver.direction)
        {
            case Direction.UP:
                targetPosition = transform.position + new Vector3(0, maneuver.distance, 0);
                break;
            case Direction.DOWN:
                targetPosition = transform.position + new Vector3(0, -maneuver.distance, 0);
                break;
            case Direction.RIGHT:
                characterAnimation.facingLeft = false;
                targetPosition = transform.position + new Vector3(maneuver.distance, 0, 0);
                break;
            case Direction.LEFT:
                characterAnimation.facingLeft = true;
                targetPosition = transform.position + new Vector3(-maneuver.distance, 0, 0);
                break;
        }
    }
}
