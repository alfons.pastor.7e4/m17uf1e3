﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            GameManager.Instance.PlayerHit();
            GameManager.Instance.DestroyedEnemy();
            Destroy(gameObject);
        }
        if (collision.gameObject.tag == "Enemy")
        {
            GameObject pl = collision.gameObject;
            GameManager.Instance.DestroyedEnemy(1, false);
            Destroy(pl);
            Destroy(gameObject);
        }

    }
}
