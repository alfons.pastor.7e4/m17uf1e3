﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }
    public int score = 0;
    public int pointsPerEnemy = 5;
    public UIController uicontroller;
    private PlayerData player;
    private Vector3 playerInitialPosition = new Vector3(2, -0.5f, 0);
    public float maxY = 0;
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);

        } else {
            
            _instance = this; 
             DontDestroyOnLoad(gameObject);
        }
    }
    private void Start()
    {
        uicontroller = GameObject.Find("Canvas").GetComponent<UIController>();
        player = GameObject.Find("Character").GetComponent<PlayerData>();
    }

    public void Update()
    {
        if (player != null)
        {
            if (player.transform.position.y > maxY)
            {
                maxY = player.transform.position.y;
                uicontroller.UpdateMaxY();
            }
        }
    }
    public void DestroyedEnemy(int n = 1, bool playerAction = true)
    {
        if (playerAction)
        {
            score = score + n*pointsPerEnemy;
            uicontroller.UpdateScore();
        }
    }
    public void SpawnedEnemy()
    {
        uicontroller.UpdateEnemyCount();
    }
    public void PlayerHit()
    {
        player.lifes--;
        uicontroller.UpdateLifesCount();
        if (player.lifes > 0)
        {
            player.transform.position = playerInitialPosition;
        }
        else
        {
            SceneManager.LoadScene("GameOver");
        }
    }

    public void PlayerGetLife()
    {
        player.lifes++;
        uicontroller.UpdateLifesCount();
    }
    public int GetPlayerLifes()
    {
        return player.lifes;
    }

    public void PlayerDeath()
    {
        if(player.lifes > 0)
        {

        }
    }
}
