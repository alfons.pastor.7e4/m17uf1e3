﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GiantAbility : MonoBehaviour
{
    private GameObject character;
    private Button button;
    public float cooldown = 10;
    private float lastTime;
    public float scalingSpeed = 1.02f;
    public float maxScaleFactor = 2;
    private float maxScale;
    private bool giant = false;

    // Start is called before the first frame update
    void Start()
    {
        lastTime = -cooldown;
        character = GameObject.Find("Character");
        button = GameObject.Find("GiantButton").GetComponent<Button>();
        button.onClick.AddListener(ActivateGiantAbility);
        maxScale = character.GetComponent<PlayerData>().height * maxScaleFactor;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        BecomeGiant();
    }

    void ActivateGiantAbility()
    {
        if(Time.time - lastTime >= cooldown)
        {
            lastTime = Time.time;
            giant = true;
        }
    }
    void BecomeGiant()
    {
        if (giant)
        {
            if(!character.GetComponent<CharacterAnimation>().Grow(scalingSpeed, maxScale))
            {
                if (Time.time - lastTime >= cooldown / 2)
                {
                    giant = false;
                    character.GetComponent<CharacterAnimation>().RestoreScale();
                }
            }
        }
    }
}
