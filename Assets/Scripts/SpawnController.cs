﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    [SerializeField]
    private GameObject enemy;
    [SerializeField]
    private int margin = 1;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnEnemy());
    }

    // Update is called once per frame
    void Update()
    {

    }
    IEnumerator SpawnEnemy()
    {
        while (true)
        {
            yield return new WaitForSeconds(1.0f);
            System.Random rnd = new System.Random();
            Vector3 stageDimensions = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
            float len = stageDimensions.x < stageDimensions.y ? stageDimensions.x : stageDimensions.y;
            bool coord = rnd.Next(0, 2) == 1;
            if (coord)
            {
                Instantiate(enemy, new Vector3(Random.RandomRange(-stageDimensions.x, stageDimensions.x), rnd.Next(0,2)==1?stageDimensions.y-margin:-stageDimensions.y+margin, 0), Quaternion.identity);
            }
            else
            {
                Instantiate(enemy, new Vector3(rnd.Next(0, 2) == 1 ? stageDimensions.x-margin : -stageDimensions.x+margin, Random.RandomRange(-stageDimensions.y, stageDimensions.y), 0), Quaternion.identity);
            }
            GameManager.Instance.SpawnedEnemy();
        }
    }
}
