﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    private TextMeshProUGUI framesText;
    private TextMeshProUGUI lifes;
    private TextMeshProUGUI score;
    private TextMeshProUGUI enemies;
    private TextMeshProUGUI maxY;
    private void Awake()
    {
        if (SceneManager.GetActiveScene().name == "Main")
        {
            lifes = GameObject.Find("Lifes").GetComponent<TextMeshProUGUI>();
        }
        score = GameObject.Find("Score").GetComponent<TextMeshProUGUI>();
        maxY = GameObject.Find("MaxY").GetComponent<TextMeshProUGUI>();
    }
    // Start is called before the first frame update
    void Start()
    {
        if (SceneManager.GetActiveScene().name == "Main")
        {
            UpdateLifesCount();
        }
        UpdateScore();
        UpdateMaxY();
    }
    private void Update()
    {
        //UpdateEnemyCount(); //fishy solution, TODO
    }
    public void UpdateLifesCount()
    {
        lifes.SetText("Lifes: " + GameManager.Instance.GetPlayerLifes());
    }
    public void UpdateScore()
    {
        score.SetText("Score: " + GameManager.Instance.score);
    }

    public void UpdateMaxY()
    {
        maxY.SetText("Max Y: " + (int)GameManager.Instance.maxY);
    }
    public void UpdateEnemyCount()
    {
        enemies.SetText("Enemies: " + GameObject.FindGameObjectsWithTag("Enemy").Length);
    }

}
